<?php
/**
 * @file
 * Context plugin to supply above the fold as a context condition.
 */

/**
 * Expose above the fold as a context condition.
 */
class contextatf_condition_atf extends context_condition {

  function condition_values() {
    return array(1 => t('Always active'));
  }

  function editor_form($context = NULL) {
    $form = parent::editor_form($context);
    $form[1]['#title'] = t('Always active');
    $form['#weight'] = -10;
    return $form;
  }

  function execute() {
    if ($this->condition_used()) {
      foreach ($this->get_contexts() as $context) {
        if (isset($_GET['contextatf_trigger'])) {
          $this->condition_met($context);
        }
      }
    }
  }
}
