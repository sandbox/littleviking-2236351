(function($) {
  Drupal.behaviors.contextATFMakeLinksClickable = {
    attach: function(context, settings) {
      if (location.search.search('contextatf_trigger') != -1) {
        $('a').click(function() {
          parent.location = $(this).attr('href');
        });
      }
    }
  };
})(jQuery);
